//
//  SebasObjectMapper2.swift
//  PokedexGR2
//
//  Created by Stalyn Quishpe on 15/6/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.

import Foundation
import ObjectMapper

class SPokemonApiResponse2:Mappable{
    
    var resultados:[SPokemonResult2]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultados <- map["results"]
    }
}

class SPokemonResult2:Mappable {
    
    var url:String? {
        
        didSet {
            
            let bm = SBackendManager2()
            bm.getPokemon(url!)
            
        }
        
    }
    
    var urlstar: String?
    var nombrestar: String?
    var pesostar: String?
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        urlstar <- map["url"]
        nombrestar <- map["name"]
        pesostar <- map["mass"]
    }
    
}

class SPokemon2:Mappable {
    
    var id:Int?
    var nombre:String?
    var peso:Float?
    var altura:Float?
    var imagen:UIImage?
    var urlstar: String?
    var nombrestar: String?
    var pesostar: String?
    
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        nombre <- map["name"]
        peso <- map["weight"]
        altura <- map["height"]
        urlstar <- map["url"]
        nombrestar <- map["name"]
        pesostar <- map["mass"]
        pesostar <- map["peso"]
        urlstar <- map["url"]
        
        
    }
    
}

