//
//  AtrapadosTableViewCell.swift
//  PokedexGR2
//
//  Created by Stalyn Quishpe on 14/6/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class AtrapadosTableViewCell: UITableViewCell {

    
    @IBOutlet weak var IamgenAtrapado: UIImageView!
    
    @IBOutlet weak var lable1Atrapado: UILabel!
    
    @IBOutlet weak var label2Atrapado: UILabel!
    
    /*@IBOutlet weak var IamgenAtrapado: UIImageView!
    
    @IBOutlet weak var label1Atrapado: UILabel!
    
    @IBOutlet weak var label2Atrapado: UILabel!
   */
    
    var pokemon:SPokemon!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func fillData(){
        
        //idLabel.text = "\(pokemon.id!)"
        //nombreLabel.text = "\(pokemon.nombre ?? "")"
        
        lable1Atrapado.text = "\(pokemon.pesostar!)"
        label2Atrapado.text = "\(pokemon.urlstar!)"
        //nombreLabel.text = "\(pokemon.nombre ?? "")"
        
        let a:Int? = Int(lable1Atrapado.text!)
        print(a)
        
        
        // vamos a poner la descripcion aqui por ejemplo el peso podria ser la descripcoin
        //nombre
        
        let descripcionnombre = (pokemon.nombre ?? "")
        let descipcionperso = (pokemon.urlstar!)
        
        
        if pokemon.imagen == nil {
            
            let bm = SBackendManager2()
            
                        
            
            
            
            // bm.getImage((pokemon?.id)!, completionHandler: { (imageR) in
            bm.getImage(a!, completionHandler: { (imageR) in
                DispatchQueue.main.async {
                    self.IamgenAtrapado.image = imageR
                }
                
            })
            
        } else {
            
            IamgenAtrapado.image = pokemon.imagen
            
        }
        
    }
    
    
    
    
}
