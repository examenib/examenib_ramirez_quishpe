//
//  GlobalVar.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 6/9/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation


//Para definir variables globales en swift, solo es necesario crear un nuevo archivo swift y aqui definir cualquier tipo de variable

var pokemonArray = [SPokemon]()

var pokemonArray2 = [SPokemon]()

var contador:Int? {
    
    didSet{
        
        if contador == 0 {
            NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
        }
    }
    
}

