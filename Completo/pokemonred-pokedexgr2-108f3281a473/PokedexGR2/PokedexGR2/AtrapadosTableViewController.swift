//
//  AtrapadosTableViewController.swift
//  PokedexGR2
//
//  Created by Stalyn Quishpe on 14/6/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

//table View controller que controla la seccion que se carga desde el backend


import UIKit

import Alamofire

class AtrapadosTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    
    @IBOutlet weak var tablaAtrapados: UITableView!
    
    var objetoslista = [datos]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bm = SBackendManager2()
        bm.getAllPokemon()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("actualizar"), object: nil)
        
    }
    
    func reload() {
        tablaAtrapados.reloadData()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return pokemonArray.count
        return objetoslista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AtrapadosCell") as! AtrapadosTableViewCell
        //cell.pokemon = pokemonArray[indexPath.row]
        
        
        
        
        
        cell.lable1Atrapado.text = objetoslista[indexPath.row].peso 
        cell.label2Atrapado.text = objetoslista[indexPath.row].url  
        
        
        
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let bm = DescripcionViewController()
        
        bm.consultar()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("actualizar"), object: nil)
    }
    
    
    
    
    
    
}
