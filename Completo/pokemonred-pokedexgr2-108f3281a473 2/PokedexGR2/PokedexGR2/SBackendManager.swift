//
//  BackendManager.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 6/9/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

class SBackendManager{
    
    func getAllPokemon () {
        
        let url = "https://pokeapi.co/api/v2/pokemon"
        
        Alamofire.request(url).responseObject { (response: DataResponse<SPokemonApiResponse>) in
            
            let pokemonResponse = response.result.value
            
            if let sPokeArray = pokemonResponse?.resultados {
                
                contador = sPokeArray.count
                
            }
        }
    }
    
    func getPokemon(_ url:String){
        
        Alamofire.request(url).responseObject { (response: DataResponse<SPokemon>) in
            
            let spokemon = response.result.value
            
            pokemonArray += [spokemon!]
            contador = contador! - 1
        }
    }
    
    func getImage(_ id:Int, completionHandler: @escaping(UIImage)->()){
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        
    }
}




