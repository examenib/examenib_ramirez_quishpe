//
//  PokemonTableViewCell.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 6/9/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var pokemonImage: UIImageView!
    
    @IBOutlet weak var idLabel: UILabel!
    
    @IBOutlet weak var nombreLabel: UILabel!
    
    var pokemon:SPokemon!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(){
        
        idLabel.text = "\(pokemon.id!)"
        nombreLabel.text = "\(pokemon.nombre ?? "")"
        
        if pokemon.imagen == nil {
            
            let bm = SBackendManager()
            
            bm.getImage((pokemon?.id)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.pokemonImage.image = imageR
                }
                
            })
            
        } else {
            
            pokemonImage.image = pokemon.imagen
            
        }
        
    }

}
