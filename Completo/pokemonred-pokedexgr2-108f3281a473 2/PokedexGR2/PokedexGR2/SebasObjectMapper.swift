//
//  SebasObjectMapper.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 6/9/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation
import ObjectMapper

class SPokemonApiResponse:Mappable{
    
    var resultados:[SPokemonResult]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultados <- map["results"]
    }
}

class SPokemonResult:Mappable {
    
    var url:String? {
        
        didSet {
            
            let bm = SBackendManager()
            bm.getPokemon(url!)
            
        }
        
    }
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        url <- map["url"]
    }
    
}

class SPokemon:Mappable {
    
    var id:Int?
    var nombre:String?
    var peso:Float?
    var altura:Float?
    var imagen:UIImage?
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        nombre <- map["name"]
        peso <- map["weight"]
        altura <- map["height"]
    }
    
}






