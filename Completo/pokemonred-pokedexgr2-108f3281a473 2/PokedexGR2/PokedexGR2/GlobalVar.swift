//
//  GlobalVar.swift
//  PokedexGR2
//
//  Created by Sebastian Guerrero on 6/9/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation


var pokemonArray = [SPokemon]()

var contador:Int? {
    
    didSet{
        
        if contador == 0 {
            NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
        }
    }
    
}

